package cadenas;

import java.util.Scanner;

/**
 *
 * @author diego
 */
public class Cadenas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Declarar una cadena:
        String cadena = "Hola";
        String cadena2 = "!!";
        
        Scanner s = new Scanner(System.in);
        
        /*
            Aunque el metodo concat() y el + realizan la misma acción,
            el operador + es mas poderoso que el metodo concat,
            ya que concat solo puede concatenar cadenas, sin embargo,
            + puede concatenar con cualquier tipo de dato.
        */
        System.out.println(cadena.concat(cadena2));
        System.out.println(cadena + " :" + 3);
        
        /*
            El metodo length() nos permite saber el numero de caracteres
            que tiene una cadena.
        */
        System.out.println(cadena + " tiene " + cadena.length() + " caracteres.");
        
        /*
            El metodo charAt regresa el caracter que se encuentra en la 
            posicion dada.
            Las posiciones comienzan en 0, por lo tanto, la ultima posicion
            es |cadena| - 1.
        */
        System.out.println("El caracter en la posicion "+  0 + " es: " + cadena.charAt(0));
        //System.out.println("El caracter en la posicion " + (cadena.length() - 1) + " es: " + cadena.charAt(cadena.length()));
        System.out.println("El caracter en la posicion " + (cadena.length() - 1) + " es: " + cadena.charAt(cadena.length() - 1));
        
        /*
            El metodo indexOf() regresa un entero que representa la posicion donde
            empieza la subcadena pasada como parametro, si subcadena no se encuentra en la
            cadena, ent. regresa -1. Y como mencionamos antes, los indices empiezan en 0.
        */
        System.out.println("La o en la cadena " + cadena + " esta en la posicion: " + cadena.indexOf("o"));
        System.out.println("La a en la cadena " + cadena + " esta en la posicion: " + cadena.indexOf("a"));
        System.out.println("La a en la cadena " + cadena + " esta en la posicion: " + cadena.indexOf("A"));
        
        /*
            El metodo contains() nos indica si una subcadena esta contenida en
            una cadena.
        */
        System.out.println("La cadena " + cadena + " contiene el caracter o? " + cadena.contains("o"));
        System.out.println("La cadena " + cadena + " contiene el caracter u? " + cadena.contains("u"));
        
        /*
            El metodo equals() nos indica si una cadena es igual a otra
            caracter a caracter.
        */
        System.out.println(cadena + " es igual a " + cadena + "? " + cadena.equals(cadena));
        System.out.println(cadena + " es igual a " + "hola? " + cadena.equals("hola"));
        
        /*
            El metodo replace() nos permite cambiar/reemplazar una parte de la cadena por otra.
        */
        System.out.println("Vamos a cambiar las 'o' por ceros en la cadena " + cadena + ": " + cadena.replace("o", "0"));
        System.out.println("Vamos a cambiar las 'a' por 4 en la cadena " + cadena + ": " + cadena.replace("a", "4"));
        
        /*
            El metodo substring() regresa una subcadena de la cadena que lo llama,
            recibe como parametro la posicion desde donde se quiere tomar la subcadena
            y termina hasta el final de la cadena.
        */
        System.out.println("Una subcadena de la cadena " + cadena + " es: " + cadena.substring(2));
        
        /*
            Una variante del metodo substring es cuando recibe dos parametros, uno
            indica la posicion donde iniciara la subcadena y el otro indica el final
            de la subcadena (pero se tomara una posicion antes).
        */
        System.out.println("Una subcadena de la cadena " + cadena + " es: " + cadena.substring(1, 3));
        
        /*
            El metodo toLowerCase() pasa a minusculas toda la cadena.
        */
        System.out.println("Pasamos la cadena " + cadena + " a minusculas: " + cadena.toLowerCase());
        
        /*
            El metodo toUpperCase() pasa a mayusculas toda la cadena.
        */
        System.out.println("Pasamos la cadena " + cadena + " a mayusculas: " + cadena.toUpperCase());
        
        /*
            Para tomar cadenas de la consola podemos usar el metodo nextLine() o
            next() de la clase Scanner. La diferencia es que next() solo toma la primera
            cadena (diferente de espacio) y lo demas lo ignora.
        */
        //cadena = s.next();
        //System.out.println(cadena);
        cadena = s.nextLine();
        System.out.println(cadena);
        
        /*
            El metodo trim quita los espacios iniciales y finales.
        */
        System.out.println("trim quita los espacios iniciales y finales:" + cadena.trim());
        
        cadena = "A.B.C.D";
        
        /*
            El metodo split separa una cadena dependiendo de la subcadena pasada como parametro.
            Ademas split regresa un arreglo de cadenas.
        */
        String [] arreglo = cadena.split(".");
        
    }
    
}
